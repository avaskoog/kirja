# Kirja 📚

WIP text rendering library written in C++14 and mainly intended for games. Uses CPU rendering to output RGBA bitmaps. Meant to be dropped wholesale into a project; no linking, no dependencies (besides C/C++ standard libs), no tears! 💃

Note that this is just a subproject of my greater game engine project, and once Kirja is sufficient for my game development needs, it might fall into some neglect. I'm mainly making this library for myself, but I'm happy to share! Just a little disclaimer that this isn't the only project receiving my attention. Feel free to fork or make pull requests if you think something is missing!

## License

Kirja is built on top of the following libraries, all of which are found under `kirja/lib`:

* *HarfBuzz*
* *minibidi*
* *libunibreak*
* *stb_truetype*
* *punixml*

If you want to use this, see those libraries for their respective licenses. As for Kirja itself, it is provided "as is" with no warranties or responsibilities by the author. Use it as you wish, if you wish!

## Features done 👍

* Takes input UTF-8 strings and outputs bitmaps using TTF fonts.
* Said input can have multiple lines and various formatting.
* Advanced text rendering of most scripts, thanks to HarfBuzz; just pick a compatible font!
* Same goes for text direction: LTR, RTL or vertical.
* `<col>` tag for setting text colour.
* `<size>` tag for setting text size.
* `<font>` tag for setting font face.
* `<img>` tag for inserting custom bitmaps inline, such as gamepad button icons!
* `<ruby>` tag for basic ruby (group or mono ruby on top of base text).

### Widely cross-platform

Kirja has been tested successfully (see screenshot below) on the following platforms:

* macOS (using Apple LLVM)
* Linux (using GCC on Ubuntu)
* Windows (using MinGW-w64 on Ubuntu; also runs in Wine)
* iOS (using Apple LLVM)
* Android (using CrystaX GCC)
* Web (using Emscripten)

## To do / WIP / wishlist 👈

* Vertical text alignment options (top/middle/bottom).
* Separating left/right alignment from left/right text direction.
* Specifying bounds, with options on how to wrap text.
* Custom hooks into the rendering to apply effects et cetera.
* Transparent text. Some scripts rely on overlapping glyphs; blending each transparent glyph one by one doesn't work; I need to batch chunks.
* Expanding on ruby, for example to place it below or to the side, and maybe supporting jukugo.
* Perhaps offer options for how to store and align pixels (channel count/order, datatype, row-major/column-major, y up/down et cetera)?
* Since stb_truetype allows for direct vertex access, maybe 3D/vector graphics are also possible?

## Known issues 👎

* Apparently *minibidi* is not fully spec-compliant; might have to use another lib.
* Certain letters fail to render for some specialised fonts, probably due to HarfBuzz trying to use contextual glyphs. Needs figuring out.

# Screenshots 👀

Provided SDL2 example | In cross-platform game engine
- | -
![Screenshot from SDL2 example](examples/example_kirja_sdl2_screenshot_macos.png) | ![Screenshot from SDL2 example](examples/kirja_all.png)
