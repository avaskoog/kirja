/**
 * @author		Ava Skoog
 * @date		2018-06-11
 * @copyright	2018 Ava Skoog
 */

#ifndef KIRJA_H_
	#define KIRJA_H_

	#include <memory>
	#include <string>
	#include <map>
	#include <cstdint>
	#include <type_traits>

	namespace kirja
	{
		class Font;
		struct Rendersettings;
		using Byte = std::uint8_t;

		/**
		 * Used to provide a success state alongside a return value.
		 * In case of failure, the error string will be filled in.
		 */
		template<typename T>
		struct Result
		{
			Result() = default;
			Result(Result<T> &&) = default;

			T value;
			const bool success;
			const std::string error;
		};

		/**
		 * Specifies horizontal text alignment.
		 */
		enum class Alignment : std::uint8_t
		{
			/// @todo: Add automatic back after figuring out the best way to deal with it.
			//automatic,

			left,
			middle,
			right
		};
		
		/**
		 * Specifies alignment of inline elements like images.
		 */
		enum class Inlinement : std::uint8_t
		{
			before,
			middle,
			after
		};
		
		/**
		 * Specifies whether to render text horizontally or vertically.
		 */
		enum class Axis : std::uint8_t
		{
			horizontal,
			vertical
		};
		
		/**
		 * Specifies the exact script being used in the text.
		 */
		enum class Script : std::uint16_t
		{
			automatic,

			arabic,
			armenian,
			bengali,
			cyrillic,
			devanagari,
			georgian,
			greek,
			gujarati,
			gurmukhi,
			hangul,
			han,
			hebrew,
			hiragana,
			kannada,
			katakana,
			lao,
			latin,
			malayalam,
			oriya,
			tamil,
			telugu,
			thai,
			tibetan,
			bopomofo,
			braille,
			canadianSyllabics,
			cherokee,
			ethiopic,
			khmer,
			mongolian,
			myanmar,
			ogham,
			runic,
			sinhala,
			syriac,
			thaana,
			yi,
			deseret,
			gothic,
			oldItalic,
			buhid,
			hanunoo,
			tagalog,
			tagbanwa,
			cypriot,
			limbu,
			linearB,
			osmanya,
			shavian,
			taiLe,
			ugaritic,
			buginese,
			coptic,
			glagolitic,
			kharoshthi,
			newTaiLue,
			oldPersian,
			sylotiNagri,
			tifinagh,
			balinese,
			cuneiform,
			nko,
			phagsPa,
			phoenician,
			carian,
			cham,
			kayahLi,
			lepcha,
			lycian,
			lydian,
			olChiki,
			rejang,
			saurashtra,
			sundanese,
			vai,
			avestan,
			bamum,
			egyptianHieroglyphs,
			imperialAramaic,
			inscriptionalPahlavi,
			inscriptionalParthian,
			javanese,
			kaithi,
			lisu,
			meeteiMayek,
			oldSouthArabian,
			oldTurkic,
			samaritan,
			taiTham,
			taiViet,
			batak,
			brahmi,
			mandaic,
			chakma,
			meroiticCursive,
			meroiticHieroglyphs,
			miao,
			sharada,
			soraSompeng,
			takri,
			bassaVah,
			caucasianAlbanian,
			duployan,
			elbasan,
			grantha,
			khojki,
			khudawadi,
			linearA,
			mahajani,
			manichaean,
			mendeKikakui,
			modi,
			mro,
			nabataean,
			oldNorthArabian,
			oldPermic,
			pahawhHmong,
			palmyrene,
			pauCinHau,
			psalterPahlavi,
			siddham,
			tirhuta,
			warangCiti,
			ahom,
			anatolianHieroglyphs,
			hatran,
			multani,
			oldHungarian,
			signwriting,
			adlam,
			bhaiksuki,
			marchen,
			osage,
			tangut,
			newa,
			masaramGondi,
			nushu,
			soyombo,
			zanabazarSquare,
			dogra,
			gunjalaGondi,
			hanifiRohingya,
			makasar,
			medefaidrin,
			oldSogdian,
			sogdian
		};
		
		struct Colour
		{
			union
			{
				Byte components[4]{255, 255, 255, 255};
				struct { Byte r, g, b, a; };
			};
		};
		
		/// @todo: Do we need this anymore now that all we ever use is four channels?
		/**
		 * Template class for bitmaps to change the number of channels.
		 * Internally different channel numbers are used for rendering,
		 * but externally the user of this library is only exposed to
		 * four-channel bitmaps, typedefined simply as Bitmap.
		 */
		template<std::size_t Channelcount>
		struct BitmapWithChannelcount
		{
			static constexpr std::size_t channelcount{Channelcount};
			
			std::size_t width{0}, height{0};
			std::unique_ptr<Byte[]> buffer;
			
			const std::size_t count() const noexcept
			{
				return width * height * channelcount;
			}
			
			const std::size_t size() const noexcept
			{
				return width * height * channelcount * sizeof(Byte);
			}
		};

		/**
		 * Container of a pixel buffer with a rendered label.
		 * Also holds its width, height and number of colour channels.
		 * Each pixel is made up of four bytes (RGBA) from 0 to 255.
		 * Access individual pixel channels using the following:
		 * [y * width * channelcount + x + channel].
		 */
		using Bitmap = BitmapWithChannelcount<4>;
		
		struct Bitmapref
		{
			const Byte *data{nullptr};
			std::size_t width{0}, height{0};
		};

		/**
		 * Holds named pointers to bitmap buffers containing images
		 * that can be referenced by <img> tags in an input string to
		 * render, in order to render images inline.
		 */
		class Imagebank
		{
			public:
				/**
				 * Adds a named bitmap buffer to the bank.
				 * The data needs to be tightly packed RGBA values 0-255 in row-major order.
				 *
				 * @param name A unique name that can be referenced by <img> tags.
				 * @param data A bitmap reference struct containing a pointer to a bitmap buffer (ownership will not be taken over, nor will a copy be made; the address needs to remain valid) and its width and height.
				 */
				void add(const char *const name, const Bitmapref &data)
				{
					/// @todo: Validate the buffer and size.
					images.emplace(name, data);
				}
			
				void remove(const char *name)
				{
					auto it(images.find(name));
					
					if (it != images.end())
						images.erase(it);
				}

			private:
				std::map<std::string, Bitmapref> images;

			friend Result<Bitmap> bitmapFromInput(const char *const, const Rendersettings &);
		};
		
		enum class Fontweight : std::uint8_t
		{
			regular,
			bold,
			italic,
			boldItalic
		};
		
		class Fontbank
		{
			private:
				struct Fontdata
				{
					Font *weights[4]
					{
						nullptr,
						nullptr,
						nullptr,
						nullptr
					};
				};
			
			public:
				void add(const char *name, Font *const, const Fontweight = Fontweight::regular);
				void remove(const char *name);
			
			private:
				std::map<std::string, Fontdata> fonts;

			friend Result<Bitmap> bitmapFromInput(const char *const, const Rendersettings &);
		};
		
		/**
		 * Struct to fill in to pass settings to bitmap rendering.
		 * Everything is set to a default so you can focus on relevant fields.
		 */
		struct Rendersettings
		{
			// Pointer to a bank of fonts (required with at least one font).
			Fontbank *fontbank{nullptr};
			
			// Pointer to a bank of images (optional).
			Imagebank *imagebank{nullptr};
			
			// Name of the font in the font bank to set as default.
			std::string font;
			
			// Font size in points.
			unsigned fontsize{32};
			
			// Ruby text size as a fraction of the main font size.
			float rubysize{0.5f};
			
			// Line spacing in pixels (can be negative).
			int linespace{0};
			
			// Colour (RGBA 0-255).
			Colour colour;
			
			// Text alignment within the bounds.
			Alignment alignment{Alignment::middle}; /// @todo: Revert to automatic after adding that back.
			
			// Image alignment with regards to baseline.
			Inlinement inlinement{Inlinement::before};
			
			// Horizontal or vertical text axis.
			Axis axis{Axis::horizontal};
			
			// The script used in the text.
			Script script{Script::automatic};
			
			// The ISO 639-3 language code.
			std::string language;
		};
		
		/**
		 * Contains a loaded font to use for bitmap rendering.
		 * Use fontFromFile() or fontFromMemory() to load one.
		 */
		class Font
		{
			public:
				Font();
				Font(Font &&);
				Font &operator=(Font &&);
				~Font();

			private:
				struct Data;
				std::unique_ptr<Data> data;
			
			friend Result<Bitmap> bitmapFromInput(const char *const, const Rendersettings &);
			friend Result<Font> fontFromMemory(const Byte *const data, const std::size_t &);
		};

		/**
		 * Takes an input UTF-8 string and tries to render it to a bitmap.
		 *
		 * @param input    The input UTF-8 string to render.
		 * @param settings The settings to render the text, such as font and size.
		 *
		 * @return A result object containing the success state and resulting bitmap.
		 */
		Result<Bitmap> bitmapFromInput(const char *const input, const Rendersettings &settings);
		
		/**
		 * Tries to load a font from the specified file path.
		 *
		 * @param path The path to the file.
		 *
		 * @return A result object containing the success state and resulting font.
		 */
		Result<Font> fontFromFile(const char *const path);
		
		/**
		 * Tries to load a font from a file in memory.
		 *
		 * @param data The memory blob containing the font file in memory.
		 * @param size The byte size of the data.
		 *
		 * @return A result object containing the success state and resulting font.
		 */
		Result<Font> fontFromMemory(const Byte *const data, const std::size_t &size);
	}
#endif
