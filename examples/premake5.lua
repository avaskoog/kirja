workspace "kirja"
	location "generated"
	targetdir "build"
	objdir "obj"

	language "C++"
	flags{"C++14"}
	optimize "On"

	configurations{"Release"}
	
	sysincludedirs{"/usr/local/include", ".."}

project "kirja"
	kind "WindowedApp"
	files
	{
		"../kirja/kirja.cpp",
		"../kirja/lib/**.cpp",
		"../kirja/lib/**.cc",
		"example_kirja_sdl2.cpp"
	}
	links
	{
		"SDL2main",
		"SDL2"
	}

