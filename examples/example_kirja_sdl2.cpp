#include <SDL2/SDL.h>
#include <kirja/kirja.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include "lib/stb_image.h"

#include <iostream>
#include <chrono>
#include <cstring>

constexpr unsigned width{700}, height{700};

SDL_Window   *window  {nullptr};
SDL_Renderer *renderer{nullptr};
SDL_Texture  *texture {nullptr};

bool initSDL();
kirja::Bitmap loadImageFromFile(const char *const);
bool renderBitmapToTexture(const kirja::Bitmap &);
void displayTexture();
void deinitSDL();

class Clock
{
	public:
		Clock(const char *const action);
		void tell();

	private:
		const std::chrono::steady_clock::time_point start;
		const std::string action;
};

constexpr const char *text
{
"<font src=\"riffic\">"
"<col hex=\"#f4b642\">"
"This"
"</col>"
" is "
"<img src=\"icon\" />"
" multiline string!"
"</font>\n"

"<font src=\"mgenplus\">"
"<col hex=\"#ffffff\">"
"これは"
"<ruby><rb>日</rb><rt>に</rt><rb>本</rb><rt>ほん</rt><rb>語</rb><rt>ご</rt></ruby>"
"です〜\n"
"<ruby><rb>今日</rb><rt>きょう</rt></ruby>"
"の"
"<ruby><rb>会</rb><rt>かい</rt><rb>議</rb><rt>ぎ</rt></ruby>"
"。"
"</col>"
"</font>\n"

"<size pt=\"40\">"
"اللغة"
"<col hex=\"#bff5ff\"> inline LTR </col>"
"العربية"
"\n"
"والأرقام ١٩٩٠ أيضًا!"
"\n"
"ممم، أحب "
"<img src=\"icon\" />"
" القهوة!"
"</size>"
"\n"

"<font src=\"riffic\">"
"<size pt=\"20\">inline</size>"
" font "
"<size pt=\"60\">SiZÊ</size>\n"

"IMAGE "
"<img src=\"icon\" />"
" TAGS!"
"</font>"
};

// This is where all the Kirja stuff is done!
bool renderTextToBitmap()
{
	// Try to load some fonts and put them in a font bank.
	auto font1(kirja::fontFromFile("fonts/amiri-regular.ttf"));
	if (!font1.success)
	{
		std::cout << font1.error << '\n';
		return false;
	}
	auto font2(kirja::fontFromFile("fonts/RifficFree-Bold.ttf"));
	if (!font2.success)
	{
		std::cout << font2.error << '\n';
		return false;
	}
	auto font3(kirja::fontFromFile("fonts/rounded-mgenplus-2cp-regular.ttf"));
	if (!font3.success)
	{
		std::cout << font2.error << '\n';
		return false;
	}
	kirja::Fontbank fontbank;
	fontbank.add("amiri",    &font1.value);
	fontbank.add("riffic",   &font2.value);
	fontbank.add("mgenplus", &font3.value);

	// Create an image bank with an image to render.
	auto icon(loadImageFromFile("images/icon.png"));
	if (!icon.buffer) return false;
	kirja::Imagebank imagebank;
	imagebank.add("icon", {icon.buffer.get(), icon.width, icon.height});

	// Settings for rendering text to bitmap.
	// Everything except the font is optional and set to a sensible default,
	// but we are setting things manually to explore the possiblities!

	kirja::Rendersettings settings;
	settings.fontbank   = &fontbank;
	settings.imagebank  = &imagebank;
	settings.font       = "amiri";
	settings.fontsize   = 45;
	settings.linespace  = 10;
	settings.alignment  = kirja::Alignment::middle;
	settings.inlinement = kirja::Inlinement::middle;
	settings.axis       = kirja::Axis::horizontal;
	settings.colour     = {220, 220, 175}; // Text alpha not currently supported.

	// Since we are mixing scripts and languages in this example,
	// it is better to let kirja (HarfBuzz) guess, but here is
	// an example of what explicig values might look like:
	
	//settings.script     = kirja::Script::arabic;
	//settings.language   = "ar";
	
	// Try to render the bitmap and time it.
	Clock c{"render bitmap"};
	auto bm(bitmapFromInput(text, settings));
	if (!bm.success)
	{
		std::cout << bm.error << '\n';
		return false;
	}
	c.tell();
	std::cout << "Bitmap size: " << bm.value.width << 'x' << bm.value.height << '\n';

	// Copy the bitmap to an SDL texture to display in the window.
	if (!renderBitmapToTexture(bm.value))
		return false;
	
	return true;
}

int main()
{
	if (initSDL())
	{
		if (renderTextToBitmap())
		{
			bool running{true};
			while (running)
			{
				SDL_Event e;
				while (SDL_PollEvent(&e))
				{
					switch (e.type)
					{
						case SDL_QUIT:
							running = false;
							break;
					}
				}
				
				displayTexture();
			}
		}
	}
	
	deinitSDL();
	return 0;
}

bool initSDL()
{
	if (0 != SDL_Init(SDL_INIT_VIDEO))
		return false;
	
	SDL_Window *window{SDL_CreateWindow
	(
		"kirja",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		width,
		height,
		SDL_WINDOW_SHOWN
	)};

	if (!window)
		return false;

	renderer = SDL_CreateRenderer(window, -1, 0);

	if (!renderer)
		return false;

	SDL_SetRenderDrawColor(renderer, 10, 55, 0, 255);

	return true;
}

bool renderBitmapToTexture(const kirja::Bitmap &bm)
{
	SDL_Surface *surface{SDL_CreateRGBSurface
	(
		0,
		static_cast<int>(bm.width),
		static_cast<int>(bm.height),
		32,
		0xFF000000,
		0x00FF0000,
		0x0000FF00,
		0x000000FF
	)};

	if (!surface)
		return false;

	SDL_SetSurfaceBlendMode(surface, SDL_BLENDMODE_BLEND);
	SDL_FillRect(surface, nullptr, SDL_MapRGBA(surface->format, 255, 0, 0, 255));

	SDL_LockSurface(surface);
	for (std::size_t y{0}; y < bm.height; ++ y)
	{
		for (std::size_t x{0}; x < bm.width * 4; x += 4)
		{
			Uint32 *p(reinterpret_cast<Uint32 *>(surface->pixels) + (y * bm.width + x / bm.channelcount));
			kirja::Byte *c{bm.buffer.get() + (y * bm.width * bm.channelcount + x)};
			Uint8 r, g, b, a;
			r = *(c + 0);
			g = *(c + 1);
			b = *(c + 2);
			a = *(c + 3);
			*p = SDL_MapRGBA(surface->format, r, g, b, a);
		}
	}
	SDL_UnlockSurface(surface);

	texture = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
	SDL_FreeSurface(surface);
	
	if (!texture)
	{
		printf("RIP\n");
		return false;
	}
	
	return true;
}

void displayTexture()
{
	SDL_RenderClear(renderer);

	SDL_Rect r;
	SDL_QueryTexture(texture, nullptr, nullptr, &r.w, &r.h);
	r.x = width / 2 - r.w / 2;
	r.y = height / 2 - r.h / 2;
	SDL_RenderCopy(renderer, texture, nullptr, &r);
	
	SDL_RenderPresent(renderer);
	SDL_Delay(100);
}

void deinitSDL()
{
	if (texture)
		SDL_DestroyTexture(texture);

	if (renderer)
		SDL_DestroyRenderer(renderer);
	
	if (window)
		SDL_DestroyWindow(window);
	
	SDL_Quit();
}

kirja::Bitmap loadImageFromFile(const char *const path)
{
	kirja::Bitmap result;
	
	int w, h, channels;
	
	if (auto pixels = stbi_load(path, &w, &h, &channels, 0))
	{
		if (0 == w || 0 == h)
			std::cout << "Error loading image '" << path << "': zero width or height\n";
		else if (4 != channels)
			std::cout << "Error loading image '" << path << "': We can only load images with four channels in this example!\n";
		else
		{
			std::cout << "Loaded '" << path << "': " << w << 'x' << h << " pixels\n";
			result.width = static_cast<std::size_t>(w);
			result.height = static_cast<std::size_t>(h);
			result.buffer = std::unique_ptr<kirja::Byte[]>{new kirja::Byte[w * h * 4]};
			std::memcpy(result.buffer.get(), pixels, result.size());
			stbi_image_free(pixels);
		}
	}
	else
		std::cout << "Error loading image '" << path << "': " << stbi_failure_reason() << '\n';
	
	return result;
}

Clock::Clock(const char *const action)
:
start {std::chrono::steady_clock::now()},
action{action}
{
}

void Clock::tell()
{
	auto diff(std::chrono::steady_clock::now() - start);
	auto ms(std::chrono::duration_cast<std::chrono::nanoseconds>(diff));
	std::cout << "Time to " << action << ": " << (static_cast<double>(ms.count()) / 1000000000.0) << "s\n";
}
